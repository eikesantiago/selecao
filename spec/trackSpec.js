var track = require("../Utils/trackController");
var fileController = require("../Utils/fileController");
var fs = require('fs');

function getArray(){
    var array = [
        {
            "name": "a",
            "duration": 10
    
        },
        {
            "name": "b",
            "duration": 20
    
        }
    ]

    return array;
}


describe('defineNumberOfTracks', function(){
    it('number of track should return null', function(){
        var numberOfTracks = track.defineNumberOfTracks(null);
        expect(numberOfTracks).toBe(null);
    });
});

describe('defineNumberOfTracks', function(){
    it('number of track should return value >= 1', function(){
        var numberOfTracks = track.defineNumberOfTracks(array = getArray());
        expect(numberOfTracks).toBeGreaterThanOrEqual(1);
    });
});

describe('createTrack', function(){
    it('Should create schedule.json file', function(){
        if (fileController.verifyIfFileExists('./schedule.json')){
            fs.unlinkSync('./schedule.json');
        }
        
        track.createTrack(2,array);
        fileExists = fileController.verifyIfFileExists('./schedule.json');
        expect(fileExists).toBe(true);
    });
});

describe('searchInArray', function(){
    it('Should return a value >= 0',function(){
        value = track.searchInArray(array = getArray(),10);

        expect(value).toBeGreaterThanOrEqual(0);
    });
});

describe('searchInArray', function(){
    it('Should return null',function(){
        value = track.searchInArray(array = getArray(),1);

        expect(value).toBe(null);
    });
});

describe('sortByDuration', function(){
    it('Should the first element in array be the one with the longest duration, duration = 20',function(){
         arraySorted = track.sortByDuration(array = getArray());
        duration = arraySorted[0].duration;
        expect(duration).toBe(20);
    });
});