var fileController = require('../../Utils/fileController');


describe('verifyIfFileExists',function(){
    it('Should return true',function(){
        fileExists = fileController.verifyIfFileExists('./proposals.txt');
        expect(fileExists).toBeTruthy();
    });
});

describe('verifyIfFileExists',function(){
    it('Should return false',function(){
        fileExists = fileController.verifyIfFileExists('./theresNoFile.txt');
        expect(fileExists).toBeFalsy();
    });
});