var consumeTextFile = require('../../Utils/consumeTextFile');
var verifyFile = require('../../Utils/fileController');
var fs = require('fs');

describe('consumeTextFile',function(){
    it('should create presentation.json file',function(){
        if(verifyFile.verifyIfFileExists('./presentation.json')){
            fs.unlinkSync('./presentation.json');
        }
        consumeTextFile.consumesTextFile();
        fileExists = verifyFile.verifyIfFileExists('./presentation.json');
        expect(fileExists).toBeTruthy();
    });
});

