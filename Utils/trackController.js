var fs = require('fs');
var exports = module.exports = {};

//Define number of tracks according by the total duration of presentations  
exports.defineNumberOfTracks = function(presentation){
    var totalDuration = 0;
    if(presentation != null && presentation.length > 0){
        for(i = 0; i < presentation.length; i++){
            totalDuration += presentation[i].duration;
        }
        numberOfTracks = totalDuration/420
        if(parseInt(numberOfTracks) < numberOfTracks){
            numberOfTracks++;
        }
        return parseInt(numberOfTracks);
    }else{
        return null;
    }
}

//Order the array of presentations by the durantion descending
exports.sortByDuration = function(presentation){
    presentation.sort(function(a,b){
        return a.duration - b.duration;
    })
    presentation.reverse();

    return presentation;
}

//Search into an array for a presentation with time equal or less than the time lefet
exports.searchInArray = function(presentation, timeLeft){
    exports.sortByDuration(presentation);
    position = presentation.findIndex(index => index.duration <= timeLeft);
    if (position >= 0){
        return position;
    }else{
        return null;
    }
}

//Create tracks for the number of tracks existing 
exports.createTrack = function(numberOfTracks, presentation){
    track = [];
    if(presentation.length > 0){
        for(i = 0 ; i < numberOfTracks; i++){
            track.push({'id': i + 1, 'schedule': [] });

            morningTime = 180;
            afternoonTime = 240;
            
            var startHour = new Date();
            var afertHour = new Date();

            startHour.setUTCHours(09,00,00,00);
            afertHour.setUTCHours(13,00,00,00);
           
            while(morningTime > 0){
                position = exports.searchInArray(presentation, morningTime);
                if(position != null){
                    track[i].schedule.push({'startTime': startHour.toUTCString(), 'name': presentation[position].name, 'duration': presentation[position].duration });
                    
                    startHour.setMinutes(startHour.getMinutes() + presentation[position].duration);
                    morningTime -= presentation[position].duration;   

                    presentation.splice(position, 1)
                }else{
                    break;
                }
            }
            track[i].schedule.push({'startTime': startHour.toUTCString(), 'name': "Almoço", 'duration': 60 });
            while(afternoonTime > 0){
                position = exports.searchInArray(presentation, afternoonTime);
                if(position != null){
                    track[i].schedule.push({'startTime': afertHour.toUTCString(), 'name': presentation[position].name, 'duration': presentation[position].duration });
                    
                    afertHour.setMinutes(afertHour.getMinutes() + presentation[position].duration);     
                    afternoonTime -= presentation[position].duration;   

                    presentation.splice(position, 1);
                }else{
                    break;
                }
            }
            afertHour.setUTCHours(17,00,00,00);
            track[i].schedule.push({'startTime': afertHour.toUTCString(), 'name': "Networking", 'duration': 60 });
        }
       fs.writeFileSync('./schedule.json',JSON.stringify(track, null, 4));  
    }
}
