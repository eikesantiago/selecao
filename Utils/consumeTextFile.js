var fs = require('fs');
var exports = module.exports = {};

exports.consumesTextFile = function(){
    var data = fs.readFileSync('./proposals.txt', 'utf8');
    data = data.split("\n");
    
    var presentations =[];
    
    for(i=0; i< data.length; i++){
        //Get last word of string after the space
        var result = data[i].substring(data[i].lastIndexOf(" ") + 1);
        name = data[i].replace(result,'');
        
        //Remove the 'min' string and replace lightning for the time
        if(result != 'lightning'){
            duration = parseInt(result.replace('min',''));
        }else{
            duration = 5;
        }
        //push into array the presentations
        presentations.push({'id': i+1, 'name': name, "duration": duration });
    
    }
    //write in a file the presentation and return it into an array
    fs.writeFileSync('./presentation.json',JSON.stringify(presentations, null, 4));
    return presentations;
}